'use strict'

module.exports = {  
    require: [
        './components/test/utils/dom.js',
        'should',
        'ignore-styles',
        'babel-polyfill',
        'isomorphic-fetch',
        './components/test/register-babel.js'
    ]
}