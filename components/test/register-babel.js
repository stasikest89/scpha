module.exports = require("@babel/register")({
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
    presets: [
        [
            "@babel/preset-env",
            {
                targets: {
                    node: "current"
                }
            }
        ],
        [
            "@babel/preset-react"
        ],
        [
            '@babel/preset-typescript', 
            {
                allowNamespaces: true,
                ignore: [/node_modules\//],
                extensions: ['.js', '.jsx', '.ts', '.tsx']
            }
        ]
    ]
});