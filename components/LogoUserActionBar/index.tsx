import styles from '../../styles/LogoUserActionBar.module.css'
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Button from '@mui/material/Button';
import Search from '../Search'
import { SECTION_LOGO_USER_ACTION_BAR, COMPONENT_LOGO, COMPONENT_SEARCH, COMPONENT_SIGNUP_BUTTON, getResponsiveSections } from '../Sections'
import Logo from '../Logo'

const LogoUserActionBar = (): JSX.Element => {
    const responsiveSections = getResponsiveSections(SECTION_LOGO_USER_ACTION_BAR)

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                    {responsiveSections.includes(COMPONENT_LOGO) &&
                        <Logo />
                    }

                    {responsiveSections.includes(COMPONENT_SEARCH) &&
                        <Search />
                    }

                    {responsiveSections.includes(COMPONENT_SIGNUP_BUTTON) &&
                        <Button 
                            color="secondary"
                            variant="contained"
                            className={styles.button}>
                            
                            Sign Up
                        </Button>
                    }
                </Toolbar>
            </AppBar>
        </Box>
    )
}

export default LogoUserActionBar
