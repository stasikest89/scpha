import { createTheme } from '@mui/material/styles'

let theme = createTheme();

theme.palette.primary = {
    light: '#383838',
    main: '#333',
    dark: '#2e2e2e',
    contrastText: '#fff'
}

theme.palette.secondary = {
    light: '#fff',
    main: '#fff',
    dark: '#F8F8F8',
    contrastText: '#333'
}

theme.typography.h1 = {
    ...theme.typography.h1,
    fontWeight: 'bold',
    [theme.breakpoints.up('xs')]: {
        fontSize: '2.2rem',
    },
    [theme.breakpoints.up('lg')]: {
        fontSize: '3.5rem',
    }
}

theme.typography.h2 = {
    ...theme.typography.h2,
    fontWeight: 'bold',
    [theme.breakpoints.up('xs')]: {
        fontSize: '1.4rem',
    },
    [theme.breakpoints.up('lg')]: {
        fontSize: '1.8rem',
    }
}

theme.typography.h3 = {
    ...theme.typography.h3,
    fontWeight: 'bold',
    [theme.breakpoints.up('xs')]: {
        fontSize: '1.2rem',
    },
    [theme.breakpoints.up('lg')]: {
        fontSize: '1.4rem',
    }
}

theme.typography.h4 = {
    ...theme.typography.h4,
    fontWeight: '500',
    lineHeight: 1.5,
    [theme.breakpoints.up('xs')]: {
        fontSize: '1rem',
    },
    [theme.breakpoints.up('lg')]: {
        fontSize: '1.2rem',
    }
}

export default theme