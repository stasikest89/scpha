import Typography from '@mui/material/Typography';
import styles from '../../styles/Banner.module.css'
import Link from '@mui/material/Link';

interface BannerPropsInterface {
    title: string,
    subtitle: string,
    link?: string
}

const Banner = ({ title, subtitle, link }: BannerPropsInterface): JSX.Element => {
    return (
        <div className={styles.container}>
            <div className={styles.content}>
                <Typography
                    variant="subtitle1"
                    component="span"
                    className={styles.text}>

                    {subtitle}
                </Typography>

                <Typography
                    variant="h1"
                    component="h1"
                    className={styles.text}>

                    {title}
                </Typography>

                {link &&
                    <Link 
                        href={link} 
                        underline="none"
                        className={styles.link}>

                        Read More
                    </Link>
                }
            </div>
        </div>
    )
}

export default Banner