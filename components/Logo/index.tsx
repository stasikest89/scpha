import Typography from '@mui/material/Typography';
import styles from '../../styles/Logo.module.css'

const Logo = (): JSX.Element => {
    return (
        <Typography
            variant="h6"
            noWrap
            component="div"
            className={styles.logo}>

            {process.env.NEXT_PUBLIC_APP_NAME}
        </Typography>
    )
}

export default Logo