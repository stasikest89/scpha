import { PAGES, PageInterface } from './options'

export default function setActivePage(pageIndex: number, router: any): void {
    const pageItem:PageInterface = PAGES[pageIndex]

    if(pageItem && pageItem.url) {
        router.push(pageItem.url)
    }
}