import styles from '../../styles/Navigation.module.css'
import Tabs from './Tabs'
import { SECTION_NAVIGATION, COMPONENT_TABS, COMPONENT_DROPDOWN_MENU, COMPONENT_SEARCH, getResponsiveSections } from '../Sections'
import Search, { VARIANT_SECONDARY } from '../Search'
import DropdownMenu from './DropdownMenu'

const Navigation = (): JSX.Element => {
    const responsiveSections = getResponsiveSections(SECTION_NAVIGATION)

    return (
        <div className={styles.container}>
            {responsiveSections.includes(COMPONENT_TABS) &&
                <Tabs />
            }

            {responsiveSections.includes(COMPONENT_SEARCH) &&
                <Search 
                    variant={VARIANT_SECONDARY}
                    className={styles.search}
                />
            }

            {responsiveSections.includes(COMPONENT_DROPDOWN_MENU) &&
                <DropdownMenu />
            }
        </div>
    )
}

export default Navigation
