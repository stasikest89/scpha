import React from 'react'
import { expect } from 'chai'
import { mount, configure } from 'enzyme'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import Component from '../DropdownMenu'

configure({ adapter: new Adapter() })

let wrapper = mount(
    <Component />
)
    
describe('Tags Test', () => {
    it('It shows default components', () => {
        expect(
            wrapper.find('button')
        ).to.have.length(1)

        expect(
            wrapper.find('button').text()
        ).to.equals('Options')

        expect(
            wrapper.find('li')
        ).to.have.length(0)
    })

    it('It shows menu on select', () => {
        wrapper
            .find('button')
            .simulate('click')

        expect(
            wrapper.find('li')
        ).to.have.length(3)
    })
}) 