export interface PageInterface {
    text: string,
    url: string
}

export const PAGES:Array<PageInterface> = [
    {
        text: 'Page 1',
        url: '/'
    },
    {
        text: 'Page 2',
        url: '/add-task'
    },
    {
        text: 'Page 3',
        url: '/list-task'
    }
]