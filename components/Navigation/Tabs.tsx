import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { PAGES, PageInterface } from './options'
import setActivePage from './setActivePage'
import { useRouter } from 'next/router'
import _ from 'lodash'

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

const TabsComponent = (): JSX.Element => {
    const router = useRouter()
    const pathname:string = router.pathname || ""
    const pageIndex = _.findIndex(PAGES, function(p) { return p.url == pathname });

    return (
        <Tabs 
            value={pageIndex} 
            onChange={(event:any, newValue: number) => setActivePage(newValue, router)}>

            {PAGES.map((item: PageInterface, index: number) => {
                return (
                    <Tab 
                        label={item.text}
                        key={index}
                        {...a11yProps(index)} 
                    />
                )
            })}
        </Tabs>
    )
}

export default TabsComponent
