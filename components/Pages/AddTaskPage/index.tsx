import Typography from '@mui/material/Typography';
import TaskManager from '../../TaskManager'

const AddTaskPage = (): JSX.Element => {
    return (
        <div>
            <Typography
                variant="h2"
                component="h1">

                Add Tasks Page
            </Typography>

            <TaskManager />
        </div>
    )
}

export default AddTaskPage
