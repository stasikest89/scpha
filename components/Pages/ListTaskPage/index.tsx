import Typography from '@mui/material/Typography';
import StatusList from '../../TaskManager/StatusList'

const ListTaskPage = (): JSX.Element => {
    return (
        <div>
            <Typography
                variant="h2"
                component="h1">

                List Tasks Page
            </Typography>

            <StatusList />
        </div>
    )
}

export default ListTaskPage
