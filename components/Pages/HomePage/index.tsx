import { useState, useEffect } from 'react'
import Banner from '../../Banner'
import Cards, { CardComponentInterface } from '../../Cards'

const HomePage = (): JSX.Element => {
    const [cards, setCards] = useState<CardComponentInterface[]>([])

    useEffect(() => {
        setCards([
            {
                title: 'Card 1',
                description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
                link: '#',
                id: 'card_id_1'
            },
            {
                title: 'Card 2',
                description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
                link: '#',
                id: 'card_id_2'
            }
        ])
    }, [])

    return (
        <div>
            <Banner 
                title="Banner for the website"
                subtitle="Subtitle"
                link="#"
            />

            <Cards cards={cards}/>
        </div>
    )
}

export default HomePage
