import Logo from '../Logo'
import Typography from '@mui/material/Typography';
import styles from '../../styles/Footer.module.css'
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import useMediaQuery from '@mui/material/useMediaQuery';

interface RenderLinkPropsInterface {
    text: string
}

const RenderLink = ({ text }: RenderLinkPropsInterface): JSX.Element => {
    return (
        <Link 
            href="#" 
            underline="none">
            
            {text}
        </Link>
    )
}

const Footer = (): JSX.Element => {
    const md = useMediaQuery((theme: any) => theme.breakpoints.up('md'))

    return (
        <Grid 
            container 
            spacing={2}
            className={styles.container}>
                
            <Grid 
                item 
                xs={12} 
                md={6}
                className={styles.blogPost}>

                <Logo />

                <Typography
                    variant="subtitle1"
                    component="span"
                    className={styles.text}>

                    Latest Block Post
                </Typography>

                <Typography
                    variant="h4"
                    component="h4"
                    className={styles.text}>

                    Ready To Get Started?
                </Typography>

                <Typography
                    variant="body1"
                    component="p">

                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                </Typography>
            </Grid>

            <Grid 
                item 
                xs={12} 
                md={6}
                className={md ? styles.menuContainer : styles.menuContainerXs}>

                <div className={styles.grid}>
                    <div>
                        <RenderLink text="Product"/>
                        <RenderLink text="Product"/>
                        <RenderLink text="Product"/>
                        <RenderLink text="Product"/>
                        <RenderLink text="Product"/>
                        <RenderLink text="Product"/>
                    </div>

                    <div>
                        <RenderLink text="Company"/>
                        <RenderLink text="Company"/>
                        <RenderLink text="Company"/>
                        <RenderLink text="Company"/>
                        <RenderLink text="Company"/>
                        <RenderLink text="Company"/>
                    </div>
                </div>

                <div className={styles.text}>
                    <Link href="#">@ 2010 - 2020</Link> <Link href="#">Privacy</Link> - <Link href="#">Terms</Link>
                </div>
            </Grid>
        </Grid>
    )
}

export default Footer
