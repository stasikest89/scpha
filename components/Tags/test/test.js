import React from 'react'
import { expect } from 'chai'
import { mount, configure } from 'enzyme'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import Component from '../Complete'

configure({ adapter: new Adapter() })

let wrapper = mount(
    <Component />
)
    
describe('Tags Test', () => {
    it('It shows complete tag text', () => {
        expect(
            wrapper.text()
        ).to.equals('Complete')
    })
}) 