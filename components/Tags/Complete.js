import React from 'react'
import InfoIcon from '@mui/icons-material/Info';
import Typography from '@mui/material/Typography';
import styles from '../../styles/Tags.module.css'

const CompleteTag = () => {
    return (
        <div className={styles.complete}>
            <InfoIcon className={styles.completeIcon}/>

            <Typography
                variant="body1"
                className={styles.completeText}
                component="p">
    
                Complete
            </Typography>
        </div>
    )
}

export default CompleteTag