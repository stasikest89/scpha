import useMediaQuery from '@mui/material/useMediaQuery';

export const COMPONENT_TABS:string = 'tabs'
export const COMPONENT_DROPDOWN_MENU:string = 'dropdown_menu'
export const COMPONENT_SEARCH:string = 'search'
export const COMPONENT_LOGO:string = 'logo'
export const COMPONENT_SIGNUP_BUTTON:string = 'signup_button'

export const SECTION_NAVIGATION = 'navigation'
export const SECTION_LOGO_USER_ACTION_BAR = 'logoUserActionBar'

interface SectionInterface {
    xs: Array<string>,
    sm: Array<string>
}

interface SectionsInterface {
    [index: string]: SectionInterface
}

export function getResponsiveSections(section: string): Array<string> {
    const sm = useMediaQuery((theme: any) => theme.breakpoints.up('sm'))

    const sections:SectionsInterface = {
        [SECTION_NAVIGATION]: {
            xs: [
                COMPONENT_SEARCH,
                COMPONENT_DROPDOWN_MENU
            ],
            sm: [
                COMPONENT_TABS
            ]
        },
        [SECTION_LOGO_USER_ACTION_BAR]: {
            xs: [
                COMPONENT_LOGO,
                COMPONENT_SIGNUP_BUTTON
            ],
            sm: [
                COMPONENT_LOGO,
                COMPONENT_SEARCH,
                COMPONENT_SIGNUP_BUTTON
            ] 
        }
    }

    if(sections[section]) {
        return sm ? sections[section].sm : sections[section].xs
    }

    return []
}