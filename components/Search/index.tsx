import { styled, alpha } from '@mui/material/styles';
import SearchIcon from '@mui/icons-material/Search';
import InputBase from '@mui/material/InputBase';

interface SearchComponentPropsInterface {
    theme?: any,
    variant?: string,
    children: JSX.Element[];
}

interface SearchPropsInterface {
    variant?: string,
    className?: string
}

export const VARIANT_SECONDARY:string = 'secondary'

const SearchComponent = styled('div')(({ theme, variant }: SearchComponentPropsInterface) => {
    let background = variant == VARIANT_SECONDARY ? theme.palette.primary.main : theme.palette.common.white

    return {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: alpha(background, 0.15),
        '&:hover': {
            backgroundColor: alpha(background, 0.25),
        },
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(1),
            width: 'auto',
        }
    }
});
  
const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
        padding: theme.spacing('0.41rem', 1, '0.41rem', 0),
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '12ch',
            '&:focus': {
                width: '20ch',
            }
        }
    }
}));

const Search = ({ variant, className }: SearchPropsInterface): JSX.Element => {
    return (
        <SearchComponent 
            className={className}
            variant={variant}>

            <SearchIconWrapper>
                <SearchIcon />
            </SearchIconWrapper>
            
            <StyledInputBase
                inputProps={{ 'aria-label': 'search' }}
            />
        </SearchComponent>
    )
}

export default Search
