import styles from '../../styles/Boilerplate.module.css'
import LogoUserActionBar from '../LogoUserActionBar'
import Container from '@mui/material/Container';
import Navigation from '../Navigation'
import Footer from '../Footer'

interface BoilerplatePropsInterface {
    children: JSX.Element
}

const Boilerplate = ({ children }: BoilerplatePropsInterface): JSX.Element => {
    return (
        <div>
            <LogoUserActionBar />

            <Container>
                <div className={styles.container}>
                    <Navigation />
                    
                    {children}
                </div>

                <Footer />
            </Container>
        </div>
    )
}

export default Boilerplate
