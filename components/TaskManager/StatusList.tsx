import React from 'react'
import styles from '../../styles/TaskManager.module.css'
import { connect } from "react-redux";
import { stateInterface, TaskInterface, TASK_STATUS_COMPLETE } from '../store/reducers'
import _ from 'lodash'
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import CompleteTag from '../Tags/Complete'

interface StatusListPropsInterface {
    tasks: TaskInterface[]
}

const mapStateToProps = (state:stateInterface) => {
    return { 
        tasks: state.tasks 
    };
};

const StatusList = ({ tasks }: StatusListPropsInterface): JSX.Element => {
    return (
        <div>
            {_.isEmpty(tasks) &&
                <Typography
                    variant="body1"
                    className={styles.noTasks}
                    component="p">
        
                    No tasks entered
                </Typography>
            }

            {!_.isEmpty(tasks) &&
                <List className={styles.list}>
                    {tasks.map((tasks: TaskInterface, index: number) => {
                        return (
                            <ListItem key={tasks.id}>
                                <ListItemButton>
                                    <div className={styles.listItem}>{`${index + 1}. ${tasks.text}`}</div> 

                                    {tasks.status == TASK_STATUS_COMPLETE &&
                                         <CompleteTag />
                                    }
                                </ListItemButton>
                            </ListItem>
                        )
                    })}
                </List>
            }
        </div>
    )
}

const Component = connect(mapStateToProps)(StatusList);
export default Component;

export const StatusListShallow = (props: StatusListPropsInterface): JSX.Element => {
    return (
        <StatusList {...props}/>
    )
}