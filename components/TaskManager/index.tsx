import React, { useState } from 'react'
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import styles from '../../styles/TaskManager.module.css'
import { connect } from "react-redux";
import { stateInterface, TaskInterface, TASK_STATUS_PENDING, TASK_STATUS_COMPLETE } from '../store/reducers'
import { addtask, completeTasks } from '../store/actions'
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import _ from 'lodash'
import { v4 as uuidv4 } from 'uuid';
import { useDispatch } from 'react-redux'

interface TaskManagerPropsInterface {
    tasks: TaskInterface[]
}

const mapStateToProps = (state:stateInterface) => {
    return { 
        tasks: state.tasks 
    };
};

const TaskManager = ({ tasks }: TaskManagerPropsInterface): JSX.Element => {
    const [text, setText] = useState<string>('')
    const [selected, setSelected] = useState<Array<string>>([])
    const dispatch = useDispatch()

    return (
        <div>
            <div className={styles.textContainer}>
                <TextField 
                    label="Add Task" 
                    variant="outlined" 
                    value={text}
                    onChange={(e: any) => setText(e.target.value)}
                />

                <Button 
                    variant="contained"
                    size="large"
                    className={styles.button}
                    startIcon={<AddIcon />}
                    disabled={!text}
                    onClick={() => {
                        dispatch(addtask({ 
                            text, 
                            status: TASK_STATUS_PENDING,
                            id: uuidv4() 
                        }))

                        setText('')
                    }}>

                    Add
                </Button>
            </div>

            <FormGroup className={styles.checkboxContainer}>
                {tasks.map((item: TaskInterface, index: number) => {
                    return (
                        <FormControlLabel 
                            control={<Checkbox 
                                checked={selected.includes(item.id) || item.status == TASK_STATUS_COMPLETE} 
                                disabled={item.status == TASK_STATUS_COMPLETE}
                                onChange={(e: any) => {
                                    let updatedState = [...selected]

                                    if(e.target.checked) {
                                        setSelected([
                                            ...updatedState,
                                            item.id
                                        ])
                                    } else {
                                        updatedState = updatedState.filter((it: string) => it !== item.id)
                                        setSelected(updatedState)
                                    }
                                }}
                            />} 
                            label={item.text}
                            key={index}
                        />
                    )
                })}
            </FormGroup>

            <Button 
                disabled={_.isEmpty(selected)}
                variant="contained"
                onClick={() => {
                    dispatch(completeTasks(selected))
                    setSelected([])
                }}>
                
                Mark Complete
            </Button>
        </div>
    )
}

const Component = connect(mapStateToProps)(TaskManager);
export default Component;

export const ShallowTaskManager = (props:any) => {
    return <TaskManager {...props}/>
}