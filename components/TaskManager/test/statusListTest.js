import React from 'react'
import { expect } from 'chai'
import { mount, configure } from 'enzyme'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import { StatusListShallow } from '../StatusList'

configure({ adapter: new Adapter() })

let props = {
    tasks: []
}

let wrapper = mount(
    <StatusListShallow {...props}/>
)
    
describe('TaskManagerTest', () => {
    it('It displays empty state', () => {
        expect(
            wrapper.text()
        ).to.equals('No tasks entered')
    })

    it('It displays completed tasks', () => {
        props.tasks = [
            {
                id: 'id',
                text: 'Test 1',
                status: 'initial'
            },
            {
                id: 'id',
                text: 'Test 2',
                status: 'complete'
            }
        ]

        wrapper.setProps(props)

        expect(
            wrapper.find('li')
        ).to.have.length(2)

        expect(
            wrapper.find('li').last().text()
        ).to.equal('2. Test 2Complete')
    })
}) 