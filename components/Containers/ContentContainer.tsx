import styles from '../../styles/Container.module.css'

interface ContentContainerPropsInterface {
    children: JSX.Element
}

const ContentContainer = ({ children }: ContentContainerPropsInterface): JSX.Element => {
    return (
        <div className={styles.container}>
            {children}
        </div>
    )
}   

export default ContentContainer
