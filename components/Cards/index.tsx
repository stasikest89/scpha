import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import styles from '../../styles/Cards.module.css'
import useMediaQuery from '@mui/material/useMediaQuery';

export interface CardComponentInterface {
    title: string,
    description: string,
    link: string,
    id: string
}

interface CardsComponentInterface {
    cards: CardComponentInterface[]
}

const CardComponent = ({ title, description }: CardComponentInterface): JSX.Element => {
    return (
        <Card sx={{ width: '100%' }}>
            <CardContent>
                <Typography
                    variant="h2"
                    component="h2">

                    {title}
                </Typography>

                <Typography
                    variant="body1"
                    component="p"
                    className={styles.text}>

                    {description}
                </Typography>
            </CardContent>

            <CardActions>
                <Button size="small">Learn More</Button>
            </CardActions>
        </Card>
    )
}

const CardsComponent = ({ cards }: CardsComponentInterface): JSX.Element => {
    const md = useMediaQuery((theme: any) => theme.breakpoints.up('md'))

    return (
        <div className={md ? styles.grid : styles.gridXs}>
            {cards.map((item: CardComponentInterface) => {
                return (
                    <CardComponent 
                        {...item}
                        key={item.id}
                    />
                )
            })}
        </div>
    )
}

export default CardsComponent