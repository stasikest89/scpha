import { ADD_TASK, COMPLETE_TASKS } from "./actions";

export const TASK_STATUS_COMPLETE = 'complete'
export const TASK_STATUS_PENDING = 'pending'

export interface TaskInterface {
    text: string,
    status: "complete"|"pending",
    id: string
}

export interface stateInterface {
    tasks: TaskInterface[]
}

const initialState:stateInterface = {
    tasks: []
};
  
function rootReducer(state = initialState, action: any) {
    if (action.type === ADD_TASK) {
        state.tasks.push(action.task)
    }

    if (action.type === COMPLETE_TASKS) {
        let updatedState = [...state.tasks]
        const taskIds:Array<string> = action.taskIds || []

        taskIds.forEach((id: string) => {
            let index:number = updatedState.findIndex((item:TaskInterface) => item.id == id);
            updatedState[index] = {
                ...updatedState[index],
                status: TASK_STATUS_COMPLETE
            }
        })

        return {
            ...state,
            tasks: updatedState
        }
    }

    return state;
}
  
export default rootReducer;