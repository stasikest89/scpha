import { TaskInterface } from './reducers'

export const ADD_TASK = 'ADD_TASK'
export const COMPLETE_TASKS = 'COMPLETE_TASKS'

export function addtask(task: TaskInterface) {
    return { 
        type: ADD_TASK, 
        task 
    }
};

export function completeTasks(taskIds: Array<string>) {
    return { 
        type: COMPLETE_TASKS, 
        taskIds 
    }
};