import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import styles from '../../styles/Cards.module.css'

export interface CardComponentInterface {
    title: string,
    description: string,
    link: string,
    id: string
}

const CardComponent = ({ title, description }: CardComponentInterface): JSX.Element => {
    return (
        <Card sx={{ minWidth: 275 }}>
            <CardContent>
                <Typography
                    variant="h2"
                    component="h2">

                    {title}
                </Typography>

                <Typography
                    variant="body1"
                    component="p"
                    className={styles.text}>

                    {description}
                </Typography>
            </CardContent>

            <CardActions>
                <Button size="small">Learn More</Button>
            </CardActions>
        </Card>
    )
}

export default CardComponent