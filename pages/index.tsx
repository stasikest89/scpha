import type { NextPage } from 'next'
import Head from 'next/head'
import Boilerplate from '../components/Boilerplate'
import HomePage from '../components/Pages/HomePage'

const Home: NextPage = () => {
    return (
        <div>
            <Head>
                <title>Home</title>
                <meta name="description" content="Home" />
            </Head>

            <Boilerplate>
                <HomePage />
            </Boilerplate>
        </div>
    )
}

export default Home
