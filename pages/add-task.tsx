import type { NextPage } from 'next'
import Head from 'next/head'
import Boilerplate from '../components/Boilerplate'
import AddTaskPage from '../components/Pages/AddTaskPage'
import ContentContainer from '../components/Containers/ContentContainer'

const Home: NextPage = () => {
    return (
        <div>
            <Head>
                <title>Add Task</title>
                <meta name="description" content="Add Task" />
            </Head>

            <Boilerplate>
                <ContentContainer>
                    <AddTaskPage />
                </ContentContainer>
            </Boilerplate>
        </div>
    )
}

export default Home
