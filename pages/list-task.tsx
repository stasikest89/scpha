import type { NextPage } from 'next'
import Head from 'next/head'
import Boilerplate from '../components/Boilerplate'
import ListTaskPage from '../components/Pages/ListTaskPage'
import ContentContainer from '../components/Containers/ContentContainer'

const Home: NextPage = () => {
    return (
        <div>
            <Head>
                <title>List Task</title>
                <meta name="description" content="List Task" />
            </Head>

            <Boilerplate>
                <ContentContainer>
                    <ListTaskPage />
                </ContentContainer>
            </Boilerplate>
        </div>
    )
}

export default Home
